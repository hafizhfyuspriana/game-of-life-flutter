import 'dart:async';
import 'dart:math';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const GameOfLifeApp());
}

class GameOfLifeApp extends StatelessWidget {
  const GameOfLifeApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: GameOfLife(),
    );
  }
}

class GameOfLife extends StatefulWidget {
  const GameOfLife({super.key});

  @override
  State<GameOfLife> createState() => _GameOfLifeState();
}

class _GameOfLifeState extends State<GameOfLife> {
  late List<List<bool>> _board;
  late List<List<bool>> _nextBoard;
  late Timer _timer;
  late int _generationCount;
  late StreamController<List<int>> _liveNeighborsController;
  final int _rows = 20;
  final int _columns = 20;
  int _gameSpeed = 150; // In milliseconds
  bool _isGameRunning = false;
  bool _isBoardPaused = false;
  List<int> _liveNeighborsCount = [];

  @override
  void initState() {
    super.initState();
    _initializeBoards();
    _liveNeighborsController = StreamController<List<int>>.broadcast();
  }

  void _initializeBoards() {
    setState(() {
      _board = List.generate(_rows, (row) => List.filled(_columns, false));
      _nextBoard = List.generate(_rows, (row) => List.filled(_columns, false));
      _isGameRunning = false;
      _isBoardPaused = false;
      _generationCount = 0;
      _liveNeighborsCount = [];
    });
  }

  void _stopGame() {
    if (_isGameRunning) {
      _timer.cancel();
      setState(() {
        _isGameRunning = false;
        _isBoardPaused = true;
      });
    }
    // Do not reset the boards here
  }

  void _startOrResumeGame() {
    if (!_isGameRunning) {
      _timer = Timer.periodic(Duration(milliseconds: _gameSpeed), (timer) { // Game speed
        setState(() {
          _updateBoard();
          _generationCount++;
        });
      });
      setState(() {
        _isGameRunning = true;
        _isBoardPaused = false;
      });
    }
  }

  Stream<List<int>> get liveNeighborsStream => _liveNeighborsController.stream;

  void _updateBoard() {
    if (_isGameRunning) {
      int totalLiveNeighbors = 0;

      for (int row = 0; row < _rows; row++) {
        for (int col = 0; col < _columns; col++) {
          int neighbors = _countLivingNeighbors(row, col);
          bool isAlive = _board[row][col];
          totalLiveNeighbors += neighbors;

          if (isAlive) {
            // Any live cell with fewer than two live neighbors dies (underpopulation)
            // Any live cell with more than three live neighbors dies (overpopulation)
            _nextBoard[row][col] = neighbors == 2 || neighbors == 3;
          } else {
            // Any dead cell with exactly three live neighbors becomes a live cell (reproduction)
            _nextBoard[row][col] = neighbors == 3;
          }
        }
      }

      setState(() {
        _liveNeighborsCount.add(totalLiveNeighbors);
        _liveNeighborsController.add(_liveNeighborsCount);
      });

      // Copy the next state to the current state
      for (int row = 0; row < _rows; row++) {
        for (int col = 0; col < _columns; col++) {
          _board[row][col] = _nextBoard[row][col];
        }
      }
    }
  }

  int _countLivingNeighbors(int row, int col) {
    int count = 0;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        if (i == 0 && j == 0) continue; // Skip the current cell
        int newRow = (row + i + _rows) % _rows;
        int newCol = (col + j + _columns) % _columns;
        if (_board[newRow][newCol]) {
          count++;
        }
      }
    }
    return count;
  }

  void _handleTouchSlide(DragUpdateDetails details) {
    RenderBox renderBox = context.findRenderObject() as RenderBox;
    Offset touchPosition = renderBox.globalToLocal(details.globalPosition);

    double cellWidth = renderBox.size.width / _columns;
    double cellHeight = renderBox.size.height / _rows;

    int col = (touchPosition.dx / cellWidth).floor();
    int row = (touchPosition.dy / cellHeight).floor();

    setState(() {
      _board[row][col] = true;
    });
  }

  void _toggleCellState(int row, int col) {
    setState(() {
      _board[row][col] = !_board[row][col];
    });
  }

  void _increaseSpeed() {
    setState(() {
      _startOrResumeGame;
      _gameSpeed += 50;
    });
  }

  void _decreaseSpeed() {
    setState(() {
      _startOrResumeGame;
      _gameSpeed -= 50;
    });
  }

  List<FlSpot> generateChartData(int currentGeneration, List<int> liveNeighborsCount) {
    List<FlSpot> data = [];
    for (int i = 1; i <= currentGeneration; i++) {
      data.add(FlSpot(i.toDouble(), liveNeighborsCount[i - 1].toDouble()));
    }
    return data;
  }


  void _showGrowthChartDialog() {
    showAdaptiveDialog(
      context: context,
      builder: (dialogContext) {
        return StreamBuilder<List<int>>(
          // Step 5: Use StreamBuilder to rebuild the chart
          stream: liveNeighborsStream,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            }

            List<int> liveNeighborsCount = snapshot.data!;

            return Wrap(
              alignment: WrapAlignment.center,
              runAlignment: WrapAlignment.center,
              children: [
                Container(
                  height: MediaQuery.sizeOf(context).width,
                  width: MediaQuery.sizeOf(context).width - 30,
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: Material(
                    child: LineChart(
                      LineChartData(
                        titlesData: const FlTitlesData(
                          rightTitles: AxisTitles(
                            axisNameSize: 0,
                          ),
                          leftTitles: AxisTitles(
                            axisNameSize: 0,
                          ),
                          topTitles: AxisTitles(
                            axisNameSize: 0,
                          ),
                        ),
                        borderData: FlBorderData(
                          show: true,
                          border: Border.all(color: const Color(0xff37434d), width: 1),
                        ),
                        minX: 1,
                        maxX: _generationCount.toDouble(),
                        minY: 0,
                        maxY: liveNeighborsCount.reduce((value, element) => max(value, element)).toDouble(),
                        lineBarsData: [
                          LineChartBarData(
                            spots: generateChartData(_generationCount, liveNeighborsCount),
                            isCurved: true,
                            color: Colors.blue,
                            dotData: FlDotData(show: false),
                            belowBarData: BarAreaData(show: false),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Game of Life'),
      ),
      body: Column(
        children: [
          Expanded(
            child: GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: _columns,
              ),
              itemBuilder: (context, index) {
                int row = index ~/ _columns;
                int col = index % _columns;
                return GestureDetector(
                  onTap: () {
                    _toggleCellState(row, col);
                  },
                  onPanUpdate: _handleTouchSlide,
                  child: Container(
                    color: _board[row][col] ? Colors.blue : Colors.white,
                    margin: EdgeInsets.all(1),
                  ),
                );
              },
              itemCount: _rows * _columns,
            ),
          ),
          Text('Generation: $_generationCount'),
          TextButton(
            onPressed: _generationCount == 0 ? null : _showGrowthChartDialog,
            child: Text('See growth chart'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Speed: $_gameSpeed'),
              Column(
                children: [
                  IconButton(
                    onPressed: _gameSpeed == 1000 ? null : _increaseSpeed,
                    icon: const Icon(Icons.arrow_drop_up),
                  ),
                  IconButton(
                    onPressed: _gameSpeed == 50 ? null : _decreaseSpeed,
                    icon: const Icon(Icons.arrow_drop_down),
                  ),
                ],
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: _isGameRunning ? null : _startOrResumeGame,
                  child: Text(_isGameRunning && !_isBoardPaused ? 'Resume' : (_isBoardPaused ? 'Resume' : 'Start')),
                ),
                SizedBox(width: 16),
                ElevatedButton(
                  onPressed: _isGameRunning ? _stopGame : null,
                  child: Text(_isGameRunning ? 'Pause' : 'Paused'),
                ),
                SizedBox(width: 16),
                ElevatedButton(
                  onPressed: () {
                    _initializeBoards();
                  },
                  child: Text('Clear'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    _liveNeighborsController.close();
    super.dispose();
  }
}
